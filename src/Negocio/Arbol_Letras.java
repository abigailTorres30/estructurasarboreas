/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import util.ufps.colecciones_seed.ArbolBinario;
import util.ufps.colecciones_seed.NodoBin;
import util.varios.BTreePrinter;

/**
 *
 * @author madarme
 */
public class Arbol_Letras {

    private ArbolBinario<Character> letras = new ArbolBinario();

    public Arbol_Letras() {

        // Crear los nodos:
        NodoBin<Character> n1 = new NodoBin();
        NodoBin<Character> n2 = new NodoBin();
        NodoBin<Character> n3 = new NodoBin();
        NodoBin<Character> n4 = new NodoBin();
        NodoBin<Character> n5 = new NodoBin();
        //Insertar sus infos:
        n1.setInfo('A');
        n2.setInfo('D');
        n3.setInfo('L');
        n4.setInfo('A');
        n5.setInfo('E');
        //Crear los subárboles:
        n1.setIzq(n2);
        n1.setDer(n5);
        n2.setIzq(n3);
        n2.setDer(n4);
        //Crear el árbol:
        this.letras.setRaiz(n1);
    }

    public ArbolBinario<Character> getLetras() {
        return letras;
    }

    public void imprimir() {
        BTreePrinter.printNode(this.getLetras().getRaiz());
    }

    public int getConHojas() {
        return this.letras.getContarHojas();
    }

    /**
     * Esté método obtiene la cantidad de vocales que SON hojas
     *
     * @return un entero con la cantidad de hojas que son vocales
     */
    
    public int getContarHojasVocales() {
	return getHojasVocales(this.letras.getRaiz());
}

    
    
    private int getHojasVocales(NodoBin<Character> r) {

        if (r == null) {
            return 0;
        }
        int cnt = this.esHojaVocal(r) ? 1 : 0;
        return cnt + this.getHojasVocales(r.getIzq()) + this.getHojasVocales(r.getDer());

    }

    private boolean isVocal(Character ch) {
        return ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u'
                || ch == 'A' || ch == 'E' || ch == 'I' || ch == 'O' || ch == 'U';
    }

    private boolean esHojaVocal(NodoBin<Character> r) {
        return r != null && r.getIzq() == null && r.getDer() == null && (this.isVocal(r.getInfo()));
    }

}
