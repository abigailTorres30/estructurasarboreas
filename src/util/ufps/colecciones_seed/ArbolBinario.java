/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util.ufps.colecciones_seed;

/**
 *
 * @author madar
 */
public class ArbolBinario<T> {

    private NodoBin<T> raiz;

    public ArbolBinario() {
    }

    public NodoBin<T> getRaiz() {
        return raiz;
    }

    public void setRaiz(NodoBin<T> raiz) {
        this.raiz = raiz;
    }

    public int getContarHojas() {

            return this.getContarHojas(this.raiz);
    }

    private int getContarHojas(NodoBin<T> r) {
        if (r == null) {
            return 0;
        }
        int c = this.esHoja(r) ? 1 : 0;
        return c + this.getContarHojas(r.getIzq()) + this.getContarHojas(r.getDer());
    }

    private boolean esHoja(NodoBin<T> x) {
        return (x != null && x.getIzq() == null && x.getDer() == null);
    }
    
    /**
     * 
     * @return un entero con la cantidad de hojas que están a la derecha
     */
    public int getCanHojasDerechas()
    {
        return 0;
    }
    
    
    
}
