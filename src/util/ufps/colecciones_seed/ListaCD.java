/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util.ufps.colecciones_seed;

import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Clase que representa una lista circular doblemente enlazada con nodo
 * centinela o nodo cabecera
 *
 * @author madarme
 */
public class ListaCD<T> implements Iterable<T>{

    private NodoD<T> cabeza;
    private int tamanio = 0;

    public ListaCD() {
    
        this.crearNodoCentinela();
    }

    private void crearNodoCentinela()
    {
        //Crear nodoCabeza:
        this.cabeza = new NodoD();
        //crear sus enlaces dobles circulares:
        this.cabeza.setSig(cabeza);
        this.cabeza.setAnt(cabeza);
    }
    
    public void borrarLista() {
        this.tamanio = 0;
        this.crearNodoCentinela();

    }

    public int getTamanio() {
        return tamanio;
    }

    public void insertarInicio(T info) {
        NodoD<T> nuevo = new NodoD(info, this.cabeza.getSig(), this.cabeza);
        nuevo.getSig().setAnt(nuevo);
        this.cabeza.setSig(nuevo);
        this.tamanio++;
    }

    public void insertarFin(T info) {
        NodoD<T> nuevo = new NodoD(info, this.cabeza, this.cabeza.getAnt());
        this.cabeza.setAnt(nuevo);
        nuevo.getAnt().setSig(nuevo);
        this.tamanio++;
    }

    @Override
    public String toString() {
        String msg = "";

        for (NodoD<T> nodoActual = this.cabeza.getSig(); nodoActual != this.cabeza; nodoActual = nodoActual.getSig()) {
            msg += nodoActual.getInfo().toString() + "<->";
        }
        return "Cabeza->" + msg + "Cabeza";

    }

    public T eliminar(int i) {
        try {
            NodoD<T> nodoBorrar = this.getPos(i);
            nodoBorrar.getAnt().setSig(nodoBorrar.getSig());
            nodoBorrar.getSig().setAnt(nodoBorrar.getAnt());
            nodoBorrar.setSig(null);
            nodoBorrar.setAnt(null);
            this.tamanio--;
            return nodoBorrar.getInfo();

        } catch (Exception ex) {
            System.err.println(ex.getMessage());
        }
        return null;
    }

    private NodoD<T> getPos(int i) throws Exception {
        if (i < 0 || i >= this.tamanio) {
            throw new Exception("Índice fuera de rango");
        }

        NodoD<T> nodoPos = this.cabeza.getSig();

        while (i-- > 0) {
            nodoPos = nodoPos.getSig();

        }
        return nodoPos;
    }

    public T get(int i) {

        try {
            return this.getPos(i).getInfo();
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
            return null;
        }
    }

    public void set(int i, T datoNuevo) {

        try {
            this.getPos(i).setInfo(datoNuevo);
        } catch (Exception ex) {
            System.err.println(ex.getMessage());

        }
    }

    public boolean esVacia() {
        //return this.tamanio==0
        return this.cabeza == this.cabeza.getSig() && this.cabeza == this.cabeza.getAnt();
    }
    
    
    /**
     * Ejercicio #14 de la hoja de listas
     * 
     * Restricciones:
     *  1. NO SE PUEDE UTILIZAR VECTORES 
     *  2. NO SE PUEDE UTILIZAR LOS MÉTODOS GET, SET , GETPOS, ELIMINAR, INSERTAR INICIO O INSERTAR FIN
     * 
     * TIP: SE DEBE CREAR TANTAS LISTAS L3(NUEVAS) COMO SEAN NECESARIAS PARA LOS REEMPLAZOS
     * 
     * @param patron Lista que se debe buscar en la lista original (l2)
     * @param nueva Lista que se debe reemplazar por el patrón (l3)
     * @return  la cantidad de reemplazos que se realizaron
     */
    
    public int reemplazar (ListaCD<T> patron, ListaCD<T> nueva)
    {
        /**
         *      this=l1, patron=l2, nueva=l3
         *      :)
         */
        return 0;
    }
    
    
    public ListaCD<T> copiar() {

        ListaCD<T> l = new ListaCD();
        Iterator<T> it = this.iterator();
        while (it.hasNext()) {
            l.insertarFin(it.next());
        }

        return l;
    }

    @Override
    public Iterator<T> iterator() {
        return new IteratorLCD(this.cabeza);
    }
}
